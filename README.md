# Locale file for English in the Netherlands (en_NL)


sudo cp en_NL /usr/share/i18n/locales/en_NL

Add:

```
en_NL.ISO-8859-1 ISO-8859-1
en_NL.ISO-8859-15 ISO-8859-15
en_NL.UTF-8 UTF-8
```
to _/etc/locale.gen_

and run `sudo locale-gen`

You can check this by runing:

```
locale -a | grep en_NL
```


